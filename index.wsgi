import sys,os
path_info = [os.path.join(os.path.dirname(__file__), 'other.zip')]
sys.path = path_info+sys.path


import sae
from project.wsgi import application as app
application = sae.create_wsgi_app(app)
