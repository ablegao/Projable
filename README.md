#Projable
Projable 最初只是用在公司内部的一个简单项目管理系统。 后来在实际的尝试当中， 不停加入一些简化项目操作流程的东西，用来简化程序的操作。

#功能介绍
1. 简单快速的项目添加、分类添加
2. 快速的任务添加
3. 项目进度统计，以及成员邮件发送。 


#代码环境
python2.7 or later

django 1.5 or later 

#安装方法

##本地环境

1.修改 project/setting.py 相关信息为自己的本地环境信息 。

2. manage.py syncdb 

3. manage.py runserver


##部署到新浪sae平台

1.[创建一个应用，同时选择python运行环境](http://sae.sina.com.cn/?m=myapp&a=create)

2.进入创建的应用管理平台， 服务管理->mysql ->初始化mysql

3.应用管理-》代码管理 -》通过这里创建一个版本

4.利用svn check到本地

5.进入本地版本库

6.git clone http://git.oschina.net/ablegao/Projable.git

7.mv Projable 1 && cd 1

6.修改project/setting.py内容

7.manage.py syncdb ，并导出mysql中生成的表结构，利用sae提供的mysql管理工具，同步到sae的mysql环境。 
