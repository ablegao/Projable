#coding:utf-8

from django.forms import ModelForm
from django.contrib.auth.models import User
from django.contrib.admin import widgets
from django import forms
from django.forms import ModelChoiceField

class UsersForm(ModelForm):
    class Meta:
        model = User
        fields = ("username" , "password")
        

import models    
class ProjectAddForm(ModelForm):
    class Meta:
        model = models.Project


##user name model . 用来显示中文名称。 
class MyModelChoiceField(ModelChoiceField):
    def label_from_instance(self, obj):
        return obj.last_name+obj.first_name

class TaskForm(ModelForm):
    user = MyModelChoiceField(User.objects.filter(is_active=1) , empty_label =None , label="所属") 
    class Meta:
        model = models.Task
        fields =("id","name","doc","cate","state","level" , "start_date" , "time_long","schedule","user","is_bug")
        widgets = {
            'name':forms.Textarea()        
        }
     
