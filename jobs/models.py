#coding:utf-8
from django.db import models
from django.contrib import admin
from django.contrib.auth.models import User
from django.forms import ModelChoiceField

#用户现实名称
class UserFullName(User):
    class Meta:
        proxy = True

    def __unicode__(self):
        return self.get_full_name()
'''
项目管理
'''
class Project(models.Model):
    choices_state = (
        (1,"未开始"),
        (2,"已开始"),
        (3,"已完成"),
    )
    id = models.AutoField(primary_key=True)
    name = models.CharField(max_length=100,help_text="任务内容",verbose_name="项目名称")
    master = models.ForeignKey(UserFullName , blank=True , verbose_name="管理" )
    start_time = models.DateField(verbose_name="项目开始时间")
    state   = models.IntegerField( verbose_name="项目状态",choices=choices_state, default=1)
    def __unicode__(self):
        return "%s" % (self.name )
        


PROJECT_USER_MASTER = 1  #项目管理
PROJECT_USER_CODER =2 #编码人员
PROJECT_USER_TESTER =3 #测试人员
# 
choices_auther = (
    (PROJECT_USER_MASTER,"项目管理"),
    (PROJECT_USER_CODER,"编码人员"),
    (PROJECT_USER_TESTER,"测试人员"),
)

#用户拥有的项目
class UserProject(models.Model):
   
    proj = models.ForeignKey(Project  , verbose_name="所属项目")
    user = models.ForeignKey(User , blank=True , verbose_name="用户" )
    auther = models.IntegerField(max_length=1 ,verbose_name="身份",choices=choices_auther  )
    def __unicode__(self):
        return "%s %s%s "  % (self.proj.name ,self.user.last_name,self.user.first_name )
        
'''
项目模块
'''

class Category(models.Model):
    id = models.AutoField(primary_key=True)
    proj = models.ForeignKey(Project  , verbose_name="所属项目")
    
    name = models.CharField(max_length=100,help_text="任务内容",verbose_name="项目名称")
    def __unicode__(self):
        return "%s" % (self.name )
    class Meta:
        ordering = ["id" ]




choices_level = (
        (1, '*'),
        (2, '**'),
        (3, '***'),
    )
choices_state = (
        (1,"未处理"),
        (2,"处理中"),
        (3,"延迟处理"),
        (4,"已完成"),
)
    
choices_isbug = (
        (0,"否"),
        (1,"是"),
)

choices_schedule = (
        (0,"0%"),
        (10,"10%"),
        (20,"20%"),
        (30,"30%"),
        (40,"40%"),
        (50,"50%"),
        (60,"60%"),
        (70,"70%"),
        (80,"80%"),
        (90,"90%"),
        (100,"100%"),
)


'''
任务表
'''
class Task (models.Model):

    id = models.AutoField(primary_key=True)
    proj = models.ForeignKey(Project  , verbose_name="所属项目")
    cate = models.ForeignKey(Category , verbose_name="所属模块",null=True,blank=True)
    name = models.CharField(max_length=100,help_text="任务内容",verbose_name="任务")
    level = models.IntegerField(max_length=30 , verbose_name="优先级",choices=choices_level,default=1 )
    start_date = models.DateField(verbose_name="开始时间" ,null=True ,blank=True)
    #end_date = models.DateField(verbose_name="预计结束时间" ,null=True,blank=True)
    time_long = models.IntegerField(max_length=11 , verbose_name="预计工期(天)"  , default=1)
    schedule = models.IntegerField(max_length=11 , verbose_name="完成进度",  choices=choices_schedule , default=1)
    edit = models.DateTimeField(verbose_name="修改时间",auto_now=True  )
    state = models.IntegerField(max_length=1 , verbose_name="当前状态" ,choices=choices_state , default=1)
    doc  = models.TextField(help_text="更多任务细节。" , verbose_name="更多细节" , null=True , blank=True )
    user = models.ForeignKey(UserFullName , null=True ,verbose_name="所属" )
    is_bug = models.IntegerField(max_length=1 , verbose_name="是否为bug" ,choices=choices_isbug , default=0)
    
    def __unicode__(self):
        return "%s %s" % (self.name , self.doc)
        
    class Meta:
        ordering = ["-level" , "-start_date"]


LogType = (
    (1,'添加'),
    (2,'修改'),
    (3,"删除"),
)

class UserLog(models.Model):
    id = models.AutoField(primary_key=True )
    typ  = models.IntegerField(max_length=1 , verbose_name="类型" ,choices=LogType ,default=1)
    user = models.ForeignKey(UserFullName , verbose_name="用户" , blank=True , null =True )
    proj = models.ForeignKey(Project  , verbose_name="所属项目", blank=True , null =True)
    cate = models.ForeignKey(Category , verbose_name="所属模块", blank=True , null =True)
    task = models.ForeignKey(Task , verbose_name="相关任务" , blank=True , null =True)
    schedule = models.IntegerField(max_length=11 , verbose_name="完成进度",  choices=choices_schedule , default=1)
    state = models.IntegerField(max_length=1 , verbose_name="当前状态" ,choices=choices_state , default=1)
    time = models.IntegerField(max_length=11 , verbose_name="消耗时间(分)" , default=1)
    edit = models.DateTimeField(verbose_name="产生时间"  , auto_now=True )


class UserLogAdmin(admin.ModelAdmin):
    list_display=("task","user","proj","cate","schedule","state","typ","edit","time")
    list_filter = ("user",)

admin.site.register(UserLog , UserLogAdmin)

def AddLog(t, typ):
    log = UserLog(typ = typ,user=t.user,proj=t.proj,cate = t.cate,task=t,schedule=t.schedule,state=t.state,time=0)
    log.save()    

'''
任务备注信息。
'''
class TaskCommit(models.Model):
    id = models.AutoField(primary_key=True)
    user = models.ForeignKey(UserFullName , verbose_name="用户" )
    task = models.ForeignKey(Task , verbose_name="相关任务")
    create_time = models.DateTimeField(verbose_name="创建时间"  , auto_now=False , auto_now_add=True)
    edit_time = models.DateTimeField(verbose_name="修改时间"  , auto_now=True )
    content = models.TextField()

'''
任务表  相关管理功能。 
'''
class TaskAdmin(admin.ModelAdmin):
    list_display=(   "name" ,"user","level" ,  "start_date","state" , "proj" , "cate",)
    search_fields = ("name",)
    list_filter = ("user","proj",)
    ordering=("level" , "-edit")
    
admin.site.register(Project)
admin.site.register(UserProject)

admin.site.register(Category)
admin.site.register(Task , TaskAdmin )

admin.site.register(TaskCommit )

    
    
    
