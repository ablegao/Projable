
from django.conf.urls import patterns, include, url

# Uncomment the next two lines to enable the admin:
from django.contrib import admin
admin.autodiscover()


import jobs.views
from django.views.generic import RedirectView

import view 

urlpatterns = patterns('',
    
    # Examples:
    # url(r'^$', 'project.views.home', name='home'),
    # url(r'^project/', include('project.foo.urls')),

    # Uncomment the admin/doc line below to enable admin documentation:
    # url(r'^admin/doc/', include('django.contrib.admindocs.urls')),

    # Uncomment the next line to enable the admin:
    url(r'^admin/', include(admin.site.urls)),
    url(r"^jobs/" , include(jobs.views.urlpatterns) , name="jobs"),
    url(r"^login$" , view.Login,{"template_file":"login.html"}),
    url(r"^logout$" , view.Logout),
    url(r"^user_edit$" , view.EditUserInfo ,{"template_file":"user_edit.html"} ),
    url(r"^user_passwd$" , view.ChangePassword ,{"template_file":"user_passwd.html"}  ),
    url(r'^user/report/([a-z]+)$' , view.UserReport ,{"template_file":"user_report.html"} ),
    url(r"^$", RedirectView.as_view(url='/jobs')),

)
